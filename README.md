# PDF Loader

## Install all pre-requirements
```
brew install freetype ghostscript imagemagick@6

# change symlink if newer imagemagick was installed (Mac OS Sierra issue)
ln -s /usr/local/Cellar/imagemagick@6/<your specific 6 version>/lib/libMagickWand-6.Q16.dylib /usr/local/lib/libMagickWand.dylib

pip install virtualenv
```

## Install pip requirements
```
mkdir %project_folder%
cd %project_folder%
git clone git@bitbucket.org:tilzaer/pdf_loader.git .
virtualenv ve
source ./ve/bin/activate
pip install -r ./requirements.txt

# [optional] create your own database
brew install sqlite3
sqlite3 test.db
> .quit
```

## Run
```
python ./main.py --port=8001
open in browser 'localhost:8001'
```