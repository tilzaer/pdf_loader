from datetime import datetime
from peewee import (
    BlobField,
    CharField,
    DateTimeField,
    ForeignKeyField,
    Model,
    SqliteDatabase,
)


db = SqliteDatabase('test.db')


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    username = CharField(unique=True)
    password = CharField()


class Pdf(BaseModel):
    user = ForeignKeyField(User, backref='pdfs')
    filename = CharField()
    date_uploaded = DateTimeField(default=datetime.now())
    data = BlobField()


class Png(BaseModel):
    pdf = ForeignKeyField(Pdf, backref='pngs')
    filename = CharField()
    data = BlobField()
