import os

from tornado import (
    httpserver,
    ioloop,
    web
)
from tornado.options import define, options

from handlers.handlers import (
    AuthSignInHandler,
    AuthSignOutHandler,
    AuthSignUpHandler,
    DownloadPDFHandler,
    DownloadPNGHandler,
    HomeHandler
)
from models.models import (
    db,
    Pdf,
    Png,
    User
)


define("port", default=8001, help="run on the given port", type=int)


class Application(web.Application):
    def __init__(self):
        handlers = [
            web.url(r'/', HomeHandler, name='home'),
            web.url(r'/auth/signup', AuthSignUpHandler, name='signup'),
            web.url(r'/auth/signin', AuthSignInHandler, name='signin'),
            web.url(r'/auth/signout', AuthSignOutHandler, name='signout'),
            web.url(r'/pdf/([0-9]+)', DownloadPDFHandler, name='pdf_download'),
            web.url(r'/png/([0-9]+)', DownloadPNGHandler, name='png_download'),
        ]
        settings = dict(
            title=u'PDF Loader',
            template_path=os.path.join(os.path.dirname(__file__), 'templates'),
            static_path=os.path.join(os.path.dirname(__file__), 'static'),
            xsrf_cookies=True,
            cookie_secret='SOME_RANDOM_VALUE',
            login_url='/auth/signin',
            debug=True,
        )
        super(Application, self).__init__(handlers, **settings)

        self.db = db
        self.create_tables()

    def create_tables(self):
        self.db.connect()
        db.create_tables([User, Pdf, Png])
        self.db.close()


def main():
    options.parse_command_line()
    http_server = httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    ioloop.IOLoop.current().start()


if __name__ == '__main__':
    main()
