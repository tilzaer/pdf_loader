import bcrypt
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
from io import BytesIO
import os
from PyPDF2 import PdfFileReader, PdfFileWriter
from tornado import escape, gen, web
from tornado.concurrent import run_on_executor
from wand.image import Image

from models.models import User, Pdf, Png


class BaseHandler(web.RequestHandler):
    def prepare(self):
        self.db.connect()
        return super(BaseHandler, self).prepare()

    def on_finish(self):
        if not self.db.is_closed():
            self.db.close()
        return super(BaseHandler, self).on_finish()

    @property
    def db(self):
        return self.application.db

    def get_current_user(self):
        user_id = self.get_secure_cookie('pdf_loader')
        if not user_id:
            return None

        try:
            user = User.get_by_id(int(user_id))
        except (User.DoesNotExist, ValueError):
            user = None

        return user


class DownloadFileHandler(BaseHandler):
    model = None

    @web.authenticated
    def get(self, *args, **kwargs):
        if self.model is None:
            raise NotImplementedError

        try:
            file = self.model.get_by_id(int(args[0]))
        except (self.model.DoesNotExist, ValueError):
            file = None

        if file is None:
            raise web.HTTPError(404)

        self.set_header('Content-Type', 'application/force-download')
        self.set_header('Content-Disposition', 'attachment; filename=' + file.filename)
        self.write(file.data)
        self.finish()


class DownloadPDFHandler(DownloadFileHandler):
    model = Pdf


class DownloadPNGHandler(DownloadFileHandler):
    model = Png


class HomeHandler(BaseHandler):
    executor = ThreadPoolExecutor(max_workers=os.cpu_count())

    def table_content(self):
        pdfs = Pdf.select().order_by('-date_uploaded')
        items = []
        for pdf in pdfs:
            items.append({
                'datetime': pdf.date_uploaded.strftime('%Y/%m/%d %H:%M:%S'),
                'user': pdf.user.username,
                'name': pdf.filename,
                'link': self.reverse_url('pdf_download', pdf.id),
                'pngs': [{'name': png.filename,
                          'link': self.reverse_url('png_download', png.id)}
                         for png in pdf.pngs]
            })
        return items

    @run_on_executor
    def split_to_pngs(self, file):
        # NOTE: Since there are calls to C libraries, then GIL wouldn't be an issue for workers
        pngs = []

        file_buffer = BytesIO()
        file_buffer.write(file)
        pdf_reader = PdfFileReader(stream=file_buffer)

        for page_num in range(pdf_reader.getNumPages()):
            page = pdf_reader.getPage(page_num)

            pdf_writer = PdfFileWriter()
            pdf_writer.addPage(page)

            png_buffer = BytesIO()
            pdf_writer.write(png_buffer)

            img = Image(blob=png_buffer.getvalue())
            pngs.append(img)

            png_buffer.close()

        file_buffer.close()
        return pngs

    @web.authenticated
    def get(self):
        self.render('home.html', success=None, error=None, items=self.table_content())

    @web.authenticated
    @gen.coroutine
    def post(self):
        if len(self.request.files.get('file', [])) == 0:
            self.render('home.html', success=None, error='Invalid path of file', items=self.table_content())
            return

        file = self.request.files['file'][0]

        pdf = Pdf(user=self.current_user, date_uploaded=datetime.now(),
                  filename=file.filename, data=file.body)
        pdf.save()

        pngs = yield self.split_to_pngs(file.body)

        for ind, png in enumerate(pngs):
            png_filename = '_'.join([file.filename, '{0}.png'.format(str(ind+1))])
            png = Png(pdf=pdf, filename=png_filename,
                      data=png.make_blob(format='png'))
            png.save()

        self.render('home.html', success='File is uploaded successfully!',
                    error=None, items=self.table_content())


class AuthSignUpHandler(BaseHandler):
    def get(self):
        self.render('signup_user.html', success=None, error=None)

    def post(self):
        name = escape.utf8(self.get_argument('name', ''))
        password = escape.utf8(self.get_argument('password', ''))

        if len(name) == 0 or len(password) == 0:
            self.render('signup_user.html', success=None, error='Name or password must not be empty!')
            return

        if len(User.filter(username=name)) > 0:
            self.render('signup_user.html', success=None, error='User with this name already exists!')
            return

        hashed_password = bcrypt.hashpw(password, bcrypt.gensalt())
        user = User(username=name, password=hashed_password)
        user.save()

        self.set_secure_cookie('pdf_loader', str(user.get_id()))
        self.redirect(self.reverse_url('home'))


class AuthSignInHandler(BaseHandler):
    def get(self):
        self.render('signin_user.html', success=None, error=None)

    def post(self):
        name = escape.utf8(self.get_argument('name'))
        password_attempt = escape.utf8(self.get_argument('password'))

        try:
            user = User.get(username=name)
        except User.DoesNotExist:
            self.render('signin_user.html', success=None, error='Wrong name!')
            return

        if escape.utf8(user.password) == bcrypt.hashpw(password_attempt, escape.utf8(user.password)):
            self.set_secure_cookie('pdf_loader', str(user.get_id()))
            self.redirect(self.reverse_url('home'))
        else:
            self.render('signin_user.html', success=None, error='Wrong password!')


class AuthSignOutHandler(BaseHandler):
    def get(self):
        self.clear_cookie('pdf_loader')
        self.redirect(self.reverse_url('home'))
